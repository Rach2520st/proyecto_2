# PROGRAMA EVALUACIONES
Usuarios	Multiples,
Complejidad	Baja

Programa de Evaluaciones es un programa que permite la creación de guías o controles por parte de un usuario identificado como un profesor para que sus alumnos los realicen. Adicionalmente existe la opción de visualizar el número de respuestas correctas y erróneas de manera gráfica.

# Obtención del Programa
Clonar este repositorio y ejecutar "proyecto.py" dentro de la carpeta "proyecto_2".

# Prerrequisitos
- Python 3. Desarrollado y probado en 3.5.6.
- Diseñador de interfaces Glade
- Conjunto de bibliotecas multiplataforma para desarrollar interfaces gráficas GTK

# Objetivo
Ofrecer al usuario un programa amigable y sencillo que permita a profesores crear guías o controles para que sus alumnos puedan realizarlos, además esta la opción de visualizar las preguntas correctas e incorrentas de manera gráfica.

# Controles
Puede ultilizarse el mouse y el teclado númerico.

# Sobre el código
Sistema que permite dos tipos de usuarios (profesores y alumnos) crear una cuenta ingresando sus datos, los cuales seran almacenados en un archivo json. Dependiendo del tipo de usuario se desplegaran distintas ventanas (menu) las que cuentan, por lo tanto, con diferentes modalidades. Tales como;
- En el caso de que el usuario sea un profesor éste tendrá la opción de crear guías para que sus alumnos realicen y la visualización de los resultados de los alumnos. Las preguntas y respuestas del profesor son almacenadas en un archivo json para luego ser enviadas al código mediante labels que son mostrados al estudiante .
- Si por otra parte en usuario es identificado como alumno su ventana principal consistirá en las opciones de responder las guías propuestas por el docente y ver sus resultados académicos.

# Errores del programa:
- Falta implentar la opción de de mostrar los resultados obtenidos.
- 
- 

# Autores
Rachell Aravena,
Constanza Valenzuela
