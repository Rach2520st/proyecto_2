#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk
from database import Database


class inicio_sesion:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/usuario inicio sesion.glade")
        self.inicio = self.builder.get_object("ventana")
        self.inicio.connect("destroy", Gtk.main_quit)
        self.inicio.set_default_size(400, 300)
        self.inicio.show_all()

        self.entry_usuario = self.builder.get_object("ingresar_usuario")
        self.entry_contrasena = self.builder.get_object("ingresar_contraseña")
        self.boton_ingresar = self.builder.get_object("boton_ingresar")
        self.boton_ingresar.connect("clicked", self.usuario_quiere_ingresar)
        self.boton_registrarse = self.builder.get_object("boto_agregar usuario")
        self.boton_registrarse.connect("clicked", self.registrar)

    def usuario_quiere_ingresar(self, evento=None):
        usuario_ingresado = self.entry_usuario.get_text()
        contrasena_ingresada = self.entry_contrasena.get_text()

        if usuario_ingresado is None or contrasena_ingresada is None:
            return

        usuarios = Database.load("usuarios.json")

        if usuarios is None:
            self.mostrar_advertencia("hay un error en usuarios.json")
            return

        for item in usuarios:
            if item["usuario"] == usuario_ingresado and item["clave"] == contrasena_ingresada:
                if item["tipo"] == "profesor":
                    menu_profesor()
                else:
                    menu_alumno()

                return

        self.mostrar_advertencia("El usuario o contraseña están erroneos")
        return

    def mostrar_advertencia(self, texto):
        dialogo = Gtk.MessageDialog(type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK)
        dialogo.format_secondary_text(texto)

        def respuesta(dialogo, respuesta):
            dialogo.close()

        dialogo.connect("response", respuesta)
        dialogo.run()

    def registrar(self, boton=None):
        ingresar_usuario()


class ingresar_usuario:
    def __init__(self, boton):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/usuario registro.glade")
        self.ventana = self.builder.get_object("ventana")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.set_default_size(400, 300)
        self.ventana.show_all()

        boton_ok = self.builder.get_object("b_Ok")
        boton_ok.connect("clicked", self.guardar_datos_nuevo_us)
        boton_cancelar = self.builder.get_object("b_cancelar")
        boton_cancelar.connect("clicked", self.cancelar)
        # en caso de presionar aceptar se guardan los datos en archivo json

    def guardar_datos_nuevo_us(self, gfdghg=None):
        self.usuario = self.builder.get_object("nombre_usuario").get_text()
        self.rut = self.builder.get_object("RUT").get_text()
        self.correo = self.builder.get_object("correo_electronico").get_text()
        self.tipo = self.builder.get_object("tipo_de_cuenta")
        self.tipo = "profesor"  # self.tipo.get_model()[self.tipo.get_active()]
        self.clave = self.builder.get_object("contraseña").get_text()

        nuevo = {}
        nuevo["usuario"] = self.usuario
        nuevo["rut"] = self.rut
        nuevo["correo"] = self.correo0
        nuevo["tipo"] = self.tipo
        nuevo["clave"] = self.clave

        if self.usuario is None or self.usuario == "":
            self.mostrar_mensaje("pon un usuario")
            return

        if self.rut is None or self.rut == "":
            self.mostrar_mensaje("pon un rut")
            return

        if self.correo is None or self.correo == "":
            self.mostrar_mensaje("falta correo")
            return

        if self.clave is None or self.clave == "":
            self.mostrar_mensaje("no hay clave")
            return

        usuarios = Database.load("usuarios.json")

        if usuarios is None:
            self.mostrar_mensaje('ha un error con usuarios.json')
            return

        # revisar si el usuario ya esta
        for usuario in usuarios:
            if usuario["usuario"] == nuevo["usuario"]:
                self.mostrar_mensaje("ya existe el usuario")
                return

        usuarios.append(nuevo)
        Database.save("usuarios.json", usuarios)

    def mostrar_mensaje(self, texto):
        dialogo = Gtk.MessageDialog(type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK)
        dialogo.format_secondary_text(texto)

        def respuesta(dialogo, respuesta):
            dialogo.close()

        dialogo.connect("response", respuesta)
        dialogo.run()

    def cancelar(self, boton=None):
        self.ventana.destroy()


class menu_profesor:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/menu profe.glade")
        self.ventana = self.builder.get_object("ventana")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.set_default_size(400, 300)
        self.ventana.show_all()

        boton_agregarg = self.builder.get_object("agrega_guias")
        boton_agregarg.connect("clicked", self.agregar_g)
        boton_resultados = self.builder.get_object("ver_evaluaciones")
        boton_resultados.connect("clicked", self.resultados)

    def agregar_g(self, boton):
        agregar_guias(boton)


class menu_alumno:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/menu alumno.glade")
        self.ventana = self.builder.get_object("ventana")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.set_default_size(400, 300)
        self.ventana.show_all()

        boton_pendiente = self.builder.get_object("g_pendientes")
        boton_pendiente.connect("clicked", self.mostrar_guia)
        boton_resultados = self.builder.get_object("ver_evaluaciones")
        boton_resultados.connect("clicked", self.resultados)

    def mostrar_guia(self, boton=None):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/guia alumno.glade")
        self.guia = self.builder.get_object("guia")
        self.guia.connect("destroy", Gtk.main_quit)
        self.guia.set_default_size(200, 200)
        guias = Database.load("guias.json")
        #obtiene los valores entregados de la guia creada por el profesor
        self.enunciado_1.set_value("pregunta_1")
        self.enunciado_2.set_value("pregunta_2")
        self.enunciado_3.set_value("pregunta_3")
        self.uno_a.set_value("opcion_1.a")
        self.uno_b.set_value("opcion_1.b")
        self.uno_c.set_value("opcion_1.c")
        self.dos_a.set_value("opcion_2.a")
        self.dos_b.set_value("opcion_2.b")
        self.dos_c.set_value("opcion_2.c")
        self.tres_a.set_value("opcion_3.a")
        self.tres_b.set_value("opcion_3.b")
        self.tres_c.set_value("opcion_3.c")
        puntaje = 0
        if alternativa_1 == respuesta_1:
            puntaje = puntaje + 1
        else:
            puntaje = puntaje
        if alternativa_2 == respuesta_2:
            puntaje = puntaje + 1 
        else:
            puntaje = puntaje
        if alternativa_3 == respuesta_3:
            puntaje = puntaje + 1 
        else:
            puntaje = puntaje

    def resultados(self, boton=None):
        return


class agregar_guias():
    def __init__(self, boton):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("glade/creacion de guia.glade")
        self.ventana = self.builder.get_object("ventana")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.set_default_size(200, 200)
        self.ventana.show_all()
        boton_guardado = self.builder.get_object("ingresa_guia")
        boton_guardado.connect("clicked", self.guardar_guia)
    
    def guardar_guia(self):
        #crea un diccionario que contiene las preguntas de diferentes guías
        guia = {}
        pregunta_1 = {}
        pregunta_1["pregunta_1"] = self.pregunta1
        pregunta_1["opcion_1.a"] = self.alternativa1_a
        pregunta_1["opcion_1.b"] = self.alternativa1_b
        pregunta_1["opcion_1.c"] = self.alternativa1_c
        pregunta_1["respuesta_1"] = self.respuesta1
        pregunta_2 = {}
        pregunta_2["pregunta_2"] = self.pregunta2
        pregunta_2["opcion_2.a"] = self.alternativa2_a
        pregunta_2["opcion_2.b"] = self.alternativa2_b
        pregunta_2["opcion_2.c"] = self.alternativa2_c
        pregunta_2["respuesta_2"] = self.respuesta2
        pregunta_3 = {}
        pregunta_3["pregunta_3"] = self.pregunta3
        pregunta_3["opcion_3.a"] = self.alternativa3_a
        pregunta_3["opcion_3.b"] = self.alternativa3_b
        pregunta_3["opcion_3.c"] = self.alternativa3_c
        pregunta_3["respuesta_3"] = self.respuesta3

        guias = Database.load("guias.json")
        guias.append(guia)
        Database.save("guias.json", guias)


if __name__ == "__main__":
    w = inicio_sesion()
    Gtk.main()
