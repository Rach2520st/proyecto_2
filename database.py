'''
Database
========

es una utilidad que lidia con todo lo necesario para leer, escribir,
serializar y cargar archivos de texto que contengan json. lo único
que uno debe hacer es importar el módulo y usarlo:
    from database import Database

leer la base de datos:
    datos = Database.load("archivo.json")

guardar en la base de datos:
    Database.save("archivo.json", datos)


tirará error si se intenta leer una base de datos que no existe. según
la documentación de python, este código atrapa todos los errores que
pueden ocurrir para evitar el cierre forzado del programa, imprimiendo
una advertencia en la terminal.

'''

import json


class Database:
    @staticmethod
    def load(name):
        try:
            handler = open(name, 'r')
            data = json.load(handler)
            handler.close()

            return data

        except OSError as ex:
            print("File not found")
            print(ex.errno, ex.strerror)

        except json.JSONDecodeError as ex:
            print("Invalid JSON")
            print(ex.msg)

        return None

    @staticmethod
    def save(name, data):
        try:
            handler = open(name, 'w')
            json.dump(data, handler, skipkeys=True, ensure_ascii=False, indent=2)
            return True

        except OSError as ex:
            print("Cant write file")
            print(ex.errno, ex.strerror)

        except TypeError:
            print("Error: invalid type")

        except OverflowError:
            print("Error: infinite recursion")

        except ValueError:
            print("Error: out of range value, violates json spec")

        return False
